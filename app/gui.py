"""
This program is a quintessence of how you should not write code. Author sincerely apologizes, if
you have to work with it, especially modify it. This file contains GUI classes and logic. Utils.py
contains extra widgets I had to implement myself for quick-and-easy dirty fixes. Logic.py contains
math logic. Good luck, stranger.
"""
import os
import re
import sys

from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (
    QApplication, QFileDialog, QFormLayout, QHBoxLayout, QLabel, QLineEdit, QMainWindow, QMessageBox,
    QPushButton, QRadioButton, QSizePolicy, QSpacerItem, QTabWidget, QTextEdit, QVBoxLayout, QWidget
)

from logic import getGraph, getGraph2

import matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

import numpy as np

from utils import LabeledSlider, QAlphaValidator, QBoolValidator

matplotlib.use('Qt5Agg')


def base_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return base_path


def _read_html(name):
    src_pattern = re.compile(r'src="(.*?)"')
    with open(f'{base_path(name)}/{name}', 'r', encoding='utf-8') as html:
        content = html.read()
        content = src_pattern.sub(lambda m: f'src="{base_path(m.group(1))}/{m.group(1)}"', content)
        return content


class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=6, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi, facecolor='#fbfbfb')
        self.axes_w = fig.add_subplot(211)
        self.axes_w.set_title('δW')
        self.axes_ro = fig.add_subplot(212)
        self.axes_ro.set_title('ρ')
        fig.tight_layout()
        super().__init__(fig)


class RBMKMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Лабораторная работа')
        self.resize(1140, 700)
        self.setFixedSize(self.size())
        self.move(QApplication.desktop().screenGeometry().center().x() - self.width() // 2, 100)
        self.initUI()

    def initUI(self):
        self.tab_widget = QTabWidget(self)
        self.setCentralWidget(self.tab_widget)

        self.front_tab = RBMKFrontTab()
        self.tab_widget.addTab(self.front_tab, 'Главная')
        self.front_tab.to_theory.clicked.connect(lambda: self.goToTab(1))

        self.task_tab = RBMKTheoryTab()
        self.tab_widget.addTab(self.task_tab, 'Теория')
        self.tab_widget.setTabEnabled(1, False)
        self.task_tab.next_tab.clicked.connect(lambda: self.goToTab(2))

        self.main_tab = RBMKMain()
        self.tab_widget.addTab(self.main_tab, 'Рабочая среда')
        self.tab_widget.setTabEnabled(2, False)

        # self.goToTab(3)

    def goToTab(self, index):
        # unlock it first
        if not self.tab_widget.isTabEnabled(index):
            self.tab_widget.setTabEnabled(index, True)
        self.tab_widget.setCurrentIndex(index)


class RBMKFrontTab(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        # background
        self.scheme = QLabel(self)
        self.scheme.setGeometry(0, 0, 1140, 670)
        name = 'imgs/front.png'
        self.scheme.setPixmap(QPixmap(f'{base_path(name)}/{name}'))

        self.to_theory = QPushButton(self)
        self.to_theory.setText('Теория')
        self.to_theory.move(1130 - self.to_theory.width(), 660 - self.to_theory.height())


class RBMKTheoryTab(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.text = QTextEdit()
        content = _read_html('theory.html')
        self.text.setHtml(content)
        self.text.setReadOnly(True)

        self.next_tab = QPushButton()
        self.next_tab.setText('Начать')
        self.next_tab.move(1130 - self.next_tab.width(), 660 - self.next_tab.height())

        self.spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.outer_layout = QVBoxLayout(self)
        self.inner_layout = QHBoxLayout()
        self.inner_layout.addItem(self.spacer)
        self.inner_layout.addWidget(self.next_tab)
        self.outer_layout.addWidget(self.text)
        self.outer_layout.addLayout(self.inner_layout)


class RBMKMain(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.task = QTextEdit()
        self.task.setReadOnly(True)
        self.task.setHtml(_read_html('task.html'))
        # adjust height
        self.task.document().adjustSize()
        height = self.task.document().size().height()
        self.task.setFixedHeight(int(height))

        self.parameter_alpha = QLineEdit()
        self.parameter_A = QLineEdit()
        self.parameter_B = QLineEdit()
        self.parameter_alpha.setMaximumWidth(100)
        self.parameter_A.setMaximumWidth(100)
        self.parameter_B.setMaximumWidth(100)
        int_validator = QAlphaValidator(0.3, 0.8, 2)
        bool_validator = QBoolValidator()

        self.parameter_alpha.setValidator(int_validator)
        self.parameter_A.setValidator(bool_validator)
        self.parameter_B.setValidator(bool_validator)

        self.spacer_v1 = QSpacerItem(20, 30, QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.mode_manual = QRadioButton('Ручной режим')
        self.mode_manual.setChecked(True)
        self.mode_manual.toggled.connect(self.auto_mode)
        self.mode_auto = QRadioButton('Автоматический режим')

        self.spacer_v2 = QSpacerItem(20, 30, QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.reactivity = QLabel('Реактивность (в долях β)')
        self.reactivity.setAlignment(Qt.AlignCenter)

        self.slider_labels = [
            '-1', ' ', '-0.8', ' ', '-0.6', ' ', '-0.4', ' ', '-0.2', ' ',
            '0',
            ' ', '0.2', ' ', '0.4', ' ', '0.6', ' ', '0.8', ' ', '1',
        ]
        self.slider = LabeledSlider(-10, 10, labels=self.slider_labels)

        self.left_column_spacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.start = QPushButton(text='Старт')
        self.stop = QPushButton(text='Стоп')
        self.stop.setEnabled(False)

        self.graph = MplCanvas()
        self._plot_ref_w = self._plot_ref_ro = None
        self.state = 0
        self.init_plot()

        self.timer = QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.update_plot)

        self.start.clicked.connect(self.start_button_handler)
        self.stop.clicked.connect(self.stop_button_handler)

        # Layouts
        self.form_layout = QFormLayout()
        self.form_layout.addRow('Коэффициент регулирования, α:', self.parameter_alpha)
        self.form_layout.addRow('Ключ обр. связи по сист. регулирования, A:', self.parameter_A)
        self.form_layout.addRow('Ключ обр. связи по мощности, B:', self.parameter_B)

        self.buttons_layout = QHBoxLayout()
        self.buttons_layout.addWidget(self.start)
        self.buttons_layout.addWidget(self.stop)

        self.left_column = QVBoxLayout()
        self.left_column.addWidget(self.task)
        self.left_column.addLayout(self.form_layout)
        self.left_column.addItem(self.spacer_v1)
        self.left_column.addWidget(self.mode_manual)
        self.left_column.addWidget(self.mode_auto)
        self.left_column.addItem(self.spacer_v2)
        self.left_column.addWidget(self.reactivity)
        self.left_column.addWidget(self.slider)
        self.left_column.addItem(self.left_column_spacer)
        self.left_column.addLayout(self.buttons_layout)

        self.global_layout = QHBoxLayout(self)
        self.global_layout.addLayout(self.left_column)
        self.global_layout.addWidget(self.graph)

    def init_plot(self):
        if self._plot_ref_w:
            self._plot_ref_w.set_xdata([])
            self._plot_ref_w.set_ydata([])
            self._plot_ref_ro.set_xdata([])
            self._plot_ref_ro.set_ydata([])

            self.graph.axes_w.relim()
            self.graph.axes_w.autoscale_view()
            self.graph.axes_ro.relim()
            self.graph.axes_ro.autoscale_view()

            self.graph.draw()

        self.t_total = 400
        self.t_current = 0

    def update_plot(self):
        self.t_current += 5
        t = np.linspace(0, 0.03, 6)

        if self.mode_auto.isChecked():
            sol = getGraph(
                int(self.parameter_A.text()), int(self.parameter_B.text()), float(self.parameter_alpha.text()),
                self.y0, t,
            )
        else:
            sol = getGraph2(
                int(self.parameter_A.text()), int(self.parameter_B.text()), float(self.parameter_alpha.text()),
                self.slider.sl.value(), self.y0, t,
            )

        self.y0 = [sol['W'][-1], sol['C'][-1], sol['RO'][-1], sol['Z'][-1]]
        if self.mode_auto.isChecked():
            ro = round(sol['RO'][-1] / 100 / 0.0065)
            if ro > 10:
                ro = 10
            if ro < -10:
                ro = -10
            self.slider.sl.setValue(ro)
        self.y['W'] += sol['W'].tolist()[1:]
        self.y['C'] += sol['C'].tolist()[1:]
        self.y['Z'] += sol['Z'].tolist()[1:]
        self.y['RO'] += sol['RO'].tolist()[1:]
        t = np.linspace(0, 0.005 * (self.t_current + 1), self.t_current + 1)

        if self._plot_ref_w:
            self._plot_ref_w.set_xdata(t)
            self._plot_ref_w.set_ydata(self.y['W'])
        else:
            self._plot_ref_w = self.graph.axes_w.plot(t, self.y['W'])[0]

        if self._plot_ref_ro:
            self._plot_ref_ro.set_xdata(t)
            self._plot_ref_ro.set_ydata(self.y['RO'])
        else:
            self._plot_ref_ro = self.graph.axes_ro.plot(t, self.y['RO'])[0]

        self.graph.axes_w.relim()
        self.graph.axes_w.autoscale_view()
        self.graph.axes_ro.relim()
        self.graph.axes_ro.autoscale_view()

        self.graph.draw()

    def auto_mode(self, checked):
        self.slider.setEnabled(checked)
        if checked:
            self.timer.setInterval(400)
        else:
            self.timer.setInterval(100)

    def start_button_handler(self):
        if self.state == 0:  # Start
            # validation
            if not self.parameter_A.text() or not self.parameter_B.text() or not self.parameter_alpha.text():
                QMessageBox.warning(self, 'Данные не заполнены', 'Имеются незаполненные входные параметры.')
                return
            if self.parameter_A.text() == '0' and self.parameter_B.text() == '0':
                QMessageBox.warning(self, 'Некорректные данные', 'Оба ключа не могут быть равны 0 одновременно.')
                return
            # buttons
            self.stop.setEnabled(True)
            # initial
            w = c = (1 - float(self.parameter_alpha.text())) / float(self.parameter_alpha.text())
            self.y0 = [w, c, 0, 0]
            self.y = {
                'W': [w],
                'C': [c],
                'Z': [0],
                'RO': [0],
            }
            self.slider.sl.setValue(0)
            # disable inputs
            self.parameter_alpha.setEnabled(False)
            self.parameter_A.setEnabled(False)
            self.parameter_B.setEnabled(False)
            self.mode_manual.setEnabled(False)
            self.mode_auto.setEnabled(False)
        elif self.state == 2:  # Continue
            self.stop.setText('Стоп')

        self.timer.start()
        self.start.setEnabled(False)
        self.state = 1

    def stop_button_handler(self):
        if self.state == 1:  # Stop
            self.timer.stop()
            self.start.setText('Продолжить')
            self.start.setEnabled(True)
            self.stop.setText('Сброс')
            self.state = 2
        elif self.state == 2:  # Reset
            # save plot
            msg_btn = QMessageBox.question(self, 'Сброс графиков', 'Вы хотите сохранить результат?')
            if msg_btn == QMessageBox.Yes:
                name, _ = QFileDialog.getSaveFileName(self, 'Сохранение графиков', filter='Изображение (*.png)')
                if name:
                    if not name.endswith('.png'):
                        name = name + '.png'
                    self.graph.figure.savefig(name)
            # reset plot
            self.init_plot()
            self.stop.setText('Стоп')
            self.stop.setEnabled(False)
            self.start.setText('Старт')
            self.state = 0
            # enable inputs
            self.parameter_alpha.setEnabled(True)
            self.parameter_A.setEnabled(True)
            self.parameter_B.setEnabled(True)
            self.mode_manual.setEnabled(True)
            self.mode_auto.setEnabled(True)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = RBMKMainWindow()
    window.show()
    sys.exit(app.exec_())
