import numpy as np

from scipy.integrate import odeint


def system(y, t, a, b, alpha):
    w, c, ro, z = y
    dydt = [
        a * ro + ((-b * alpha * 0.017 - 0.0065) * w + 0.0065 * c) * 1000,
        0.0765 * (w - c),
        z,
        -(z + 100 * w) * 100,
    ]
    return dydt


def getGraph(a, b, alpha, y0, t):
    sol = odeint(system, y0, t, args=(a, b, alpha))
    result = {
        'W': sol[:, 0],
        'C': sol[:, 1],
        'RO': sol[:, 2],
        'Z': sol[:, 3],
    }
    return result


def system2(y, t, a, b, ro, alpha):
    w, c = y
    dydt = [
        a * ro + ((-b * alpha * 0.017 - 0.0065) * w + 0.0065 * c) * 1000,
        0.0765 * (w - c),
    ]
    return dydt


def getGraph2(a, b, alpha, ro, y0, t):
    sol = odeint(system2, y0[:2], t, args=(a, b, ro, alpha))
    result = {
        'W': sol[:, 0],
        'C': sol[:, 1],
        'RO': np.array([ro] * len(sol[:, 0])),
        'Z': np.array([0] * len(sol[:, 0])),
    }
    return result
