## Полезные ссылки

* [Общая ссылка на папку на диске](https://drive.google.com/drive/folders/1izoOEFYmJukDQ0ZvaUQg7HReKTotAH-L?usp=sharing)
* [Динамика ядерных энергоблоков](https://drive.google.com/file/d/18axSC4dCj_S2Zz8K8iErZRd74bWW4GMJ/view?usp=sharing)
* [Пример работы](https://drive.google.com/drive/folders/1iOUaZF_vOAJ13n_dGSSfMSc1EyiY0TFg?usp=sharing)
* [Техническое задание](https://docs.google.com/document/d/196spVOMUKKdfhXXGPiOL4jKC_n3MyDpeJIBc4Ii55lM/edit)
* [Учебное пособие из МГТУ](https://drive.google.com/file/d/1kKi-9WZWEkGPjOxT9JCFfOe7Tvh_erSb/view?usp=sharing)
* [Сепарационные устройства АЭС](https://drive.google.com/file/d/1pb0wH-3Ja06ZUBpiPRV-rgx35VlboyBL/view?usp=sharing)